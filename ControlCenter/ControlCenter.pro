#-------------------------------------------------
#
# Project created by QtCreator 2018-06-04T20:16:04
#
#-------------------------------------------------

QT += core
QT += widgets
QT -= gui
QT += network websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11
TARGET = ControlCenter
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

HEADERS += defines.h \
    MediaCenter/audio/audiodecoder.h \
    MediaCenter/video/decoder/decoder.h \
    NetWork/socket/socket.h \
    ServerLogic/server.h \
    UILogic/imagelabel.h \
    UILogic/mainframe.h \
    UILogic/chatframe/chatframe.h \
    UILogic/chatframe/mutichatframe.h \
    global.h

SOURCES += main.cpp \
    MediaCenter/audio/audiodecoder.cpp \
    MediaCenter/video/decoder/decoder.cpp \
    NetWork/socket/socket.cpp \
    ServerLogic/server.cpp \
    UILogic/imagelabel.cpp \
    UILogic/mainframe.cpp \
    UILogic/chatframe/chatframe.cpp \
    UILogic/chatframe/mutichatframe.cpp

DEFINES += QT_DEPRECATED_WARNINGS
DESTDIR += ./bin
#MOC_DIR += ./tmp
#OBJECTS_DIR += ./tmp

win32 {
    #ffmpeg
    #INCLUDEPATH += $$/usr/local/ffmpeg/include
    #LIBS += $$PWD/static.a
    #ffmpeg
    #LIBS += -L/usr/local/ffmpeg/lib/ -lavcodec -lavfilter -lavutil -lavdevice -lavformat -lswscale -lswresample

    message("win32")
}

