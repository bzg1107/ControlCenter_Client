#include "UILogic/mainframe.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    mainframe w;
    w.show();

    return a.exec();
}
