This is a FFmpeg Win32 shared build by Kingsoft.

libraries from the FFmpeg project are used under the LGPLv2.1

Zeranoe's FFmpeg Builds Home Page: <http://ffmpeg.zeranoe.com/builds/>

This build was compiled on: Dec 24 2014

FFmpeg version: 2014-09-13 git-bcac0f4
  libavutil      54.  7.100 / 54.  7.100
  libavcodec     56.  1.100 / 56.  1.100
  libavformat    56.  4.101 / 56.  4.101

This FFmpeg build was configured with:
--enable-shared 
--disable-static 
--enable-libopus 
--enable-libvpx 
--toolchain=msvc 
--disable-programs 
--disable-ffplay 
--disable-ffprobe 
--disable-ffserver
--disable-network 
--disable-everything 
--enable-encoder=aac 
--enable-encoder=ac3 
--enable-encoder=libopus 
--enable-encoder=mpeg4 
--enable-encoder=msmpeg4v3 
--enable-encoder=wmav2 
--enable-encoder=libvpx_vp8 
--enable-muxer=avi 
--enable-muxer=asf 
--enable-muxer=mp4 
--enable-muxer=webm 
--enable-protocol=file 
--enable-demuxer=asf 
--enable-demuxer=avi 
--enable-demuxer=m4v 
--enable-demuxer=mp3 
--enable-demuxer=mpegps 
--enable-demuxer=mpegts 
--enable-demuxer=mpegtsraw 
--enable-demuxer=mpegvideo 
--enable-demuxer=au 
--enable-demuxer=ogg 
--enable-demuxer=wav 
--enable-demuxer=xwma 
--enable-demuxer=mov 
--enable-decoder=h264 
--enable-decoder=metasound 
--enable-decoder=mpeg1video 
--enable-decoder=mpeg2video 
--enable-decoder=mpegvideo 
--enable-decoder=mpeg4 
--enable-decoder=msmpeg4v1 
--enable-decoder=msmpeg4v2 
--enable-decoder=msmpeg4v3 
--enable-decoder=msvideo1 
--enable-decoder=vp8 
--enable-decoder=wmv1 
--enable-decoder=wmv2 
--enable-decoder=wmv3 
--enable-decoder=aac 
--enable-decoder=ac3 
--enable-decoder=adpcm_ima_wav 
--enable-decoder=adpcm_ms 
--enable-decoder=ape 
--enable-decoder=flac 
--enable-decoder=flv 
--enable-decoder=adpcm_ms 
--enable-decoder=ape 
--enable-decoder=mp2 
--enable-decoder=mp3 
--enable-decoder=mp3float 
--enable-decoder=pcm_alaw 
--enable-decoder=pcm_mulaw 
--enable-decoder=pcm_dvd 
--enable-decoder=pcm_f32le 
--enable-decoder=pcm_s16le 
--enable-decoder=pcm_s32le 
--enable-decoder=pcm_s8 
--enable-decoder=pcm_s16le_planar 
--enable-decoder=pcm_s32le_planar 
--enable-decoder=pcm_s8_planar 
--enable-decoder=pcm_u8 
--enable-decoder=vmnc 
--enable-decoder=wavpack 
--enable-decoder=wmav1 
--enable-decoder=wmav2 
--enable-decoder=wmapro
--enable-parser=mpegvideo 
--enable-parser=mpeg4video 
--enable-parser=mpegaudio 
--enable-parser=h264 
--enable-parser=vc1 
--enable-parser=aac 
--enable-parser=ac3 
--enable-parser=opus 

This build was compiled with the following external libraries:
  opus 1.1.0 <http://opus-codec.org/>
  vpx 1.3.0 <http://webmproject.org/>

The source code for this FFmpeg build can be found at: <http://ffmpeg.zeranoe.com/builds/source/>

This build was compiled on Windows 7 x64

GCC 4.8.3 was used to compile this FFmpeg build: <http://gcc.gnu.org/>

This build was compiled using the msvc toolchain: <http://mingw-w64.sourceforge.net/>

Licenses for each library can be found in the 'licenses' folder.
