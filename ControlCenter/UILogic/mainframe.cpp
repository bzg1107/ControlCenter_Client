#include "mainframe.h"
#include "chatframe/chatframe.h"

mainframe::mainframe(QWidget *parent)
    : QWidget(parent)
{
    init();
}

mainframe::~mainframe()
{

}

void mainframe::init()
{
    m_chatFrameVector.clear();
    this->resize(300,300);
    m_bt_newChatFrame = new QPushButton("新的聊天窗口",this);
    m_bt_newChatFrame->resize(100,40);
    connect(m_bt_newChatFrame, SIGNAL(clicked(bool)), this, SLOT(onNewChatFrame(bool)));
}


//event

void mainframe::closeEvent(QCloseEvent *)
{
    QVector<ChatFrame*>::iterator it = m_chatFrameVector.begin();
    for(it; it != m_chatFrameVector.end();it++)
    {
        ChatFrame* frame = *it;
        if(frame){
            frame->deleteLater();
        }
    }
    m_chatFrameVector.clear();
}

//SLOT
void mainframe::onNewChatFrame(bool)
{
    ChatFrame* newChat = new ChatFrame();
    m_chatFrameVector.append(newChat);
    newChat->show();
}
