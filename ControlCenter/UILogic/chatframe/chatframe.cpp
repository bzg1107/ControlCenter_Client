#include "chatframe.h"
#include "defines.h"
#include "global.h"

ChatFrame::ChatFrame(QWidget *parent)
    : QWidget(parent)
{
    m_listenPort = g_ListenPort++;
    init();
    initSock();
}

ChatFrame::~ChatFrame()
{

}

void ChatFrame::init()
{
    this->resize(QSize(400,500));
    m_bt_connect = new QPushButton("连接",this);
    m_bt_connect->resize(50,30);
    m_bt_connect->move(100,460);
    QObject::connect(m_bt_connect, SIGNAL(clicked(bool)), this, SLOT(onClickedConnect(bool)));

    m_bt_sendMsg = new QPushButton("发送",this);
    m_bt_sendMsg->resize(50,30);
    QObject::connect(m_bt_sendMsg, SIGNAL(clicked(bool)), this, SLOT(onSendMsg(bool)));
    m_bt_sendMsg->move(20,460);

    m_msgShow = new QTextEdit(this);
    m_msgShow->resize(350,300);
    m_msgShow->move(20,0);
    m_msgEdit = new QTextEdit(this);
    m_msgEdit->move(20,310);
    m_msgEdit->resize(350,150);

}

void ChatFrame::initSock()
{
    m_serverSock = new QTcpServer(this);
    m_serverSock->listen(QHostAddress::Any, m_listenPort);
    QObject::connect(m_serverSock,SIGNAL(newConnection()),this,SLOT(onNewConnection()));
    m_connectSock = new QTcpSocket(this);
    QObject::connect(m_connectSock, SIGNAL(connected()), this, SLOT(onSelfConnected()));
}

void ChatFrame::connect(QString ip, unsigned short port)
{
    if(m_connectSock)
    {
        m_connectSock->connectToHost(ip, port);
    }
}

//event

void ChatFrame::closeEvent(QCloseEvent *)
{
    if(m_connectSock)
    {
        m_connectSock->close();
        m_connectSock->deleteLater();
    }
    if(m_serverSock)
    {
        m_serverSock->close();
        m_serverSock->deleteLater();
    }
}

//SLOT
void ChatFrame::onClickedConnect(bool)
{
    connect("127.0.0.1", 10087);
}

void ChatFrame::onSendMsg(bool)
{
    QString sendData(m_msgEdit->toPlainText());
    QByteArray data = sendData.toLocal8Bit();
    if(m_connectSock && m_connectSock->state() == QAbstractSocket::ConnectedState)
    {
        m_connectSock->write(data.data(), data.size());
    }
}

void ChatFrame::onNewConnection()
{
   // QTcpSocket* socket =m_serverSock->nextPendingConnection();
  //  m_msgSockVector.append(socket);
    m_curSock = m_serverSock->nextPendingConnection();
    QObject::connect(m_curSock,SIGNAL(readyRead()),this,SLOT(onReceiveData()));
    QObject::connect(m_curSock,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(msgSocketError(QAbstractSocket::SocketError)));
    QObject::connect(m_curSock,SIGNAL(disconnected()),this,SLOT(onSockDisconnected()));
}

void ChatFrame::onSelfConnected()
{
    m_msgShow->append("\nconnected");
}

void ChatFrame::onReceiveData()
{
    QTcpSocket *curSock = static_cast<QTcpSocket*>(sender());
    if(curSock){
        QByteArray rexvData = curSock->readAll();
        m_msgShow->append("\n");

        m_msgShow->append(QString::fromLocal8Bit(rexvData));
    }
}

void ChatFrame:: msgSocketError(QAbstractSocket::SocketError sockerror)
{
    qDebug()<<sockerror;
}

void ChatFrame::onSockDisconnected(){
    qDebug()<<"onSockDisconnected";
}
