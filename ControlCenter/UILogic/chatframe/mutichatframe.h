#ifndef MUTICHATFRAME_H
#define MUTICHATFRAME_H


#include<QPushButton>
#include <QWidget>
#include <QTextEdit>

/**
 * @brief 群聊界面
 */
class MutiChatFrame : public QWidget
{
    Q_OBJECT

public:
    MutiChatFrame(QWidget *parent = 0);
    ~MutiChatFrame();
    void init();
protected:
    virtual void closeEvent(QCloseEvent *);
private:
    QWidget *m_memberListWidget;
signals:
public slots:
    void onCloseFrame(bool);
    void onSendMsg(bool);
};

#endif // MUTICHATFRAME_H
