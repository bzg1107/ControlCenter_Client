#ifndef CHATFRAME_H
#define CHATFRAME_H


#include<QPushButton>
#include <QWidget>
#include <QTextEdit>
#include <QTcpServer>
#include <QTcpSocket>
#include <QVector>
/**
 * @brief 聊天界面
 */
class ChatFrame : public QWidget
{
    Q_OBJECT

public:
    ChatFrame(QWidget *parent = 0);
    ~ChatFrame();
    void init();
    void initSock();

    void connect(QString ip, unsigned short port);
protected:
    virtual void closeEvent(QCloseEvent *);
private:
    QPushButton *m_bt_connect;
    QPushButton *m_bt_sendMsg;
    QTextEdit* m_msgShow;
    QTextEdit* m_msgEdit;

    QTcpServer* m_serverSock;//监听连接
    QTcpSocket* m_connectSock; //主动连接
    QTcpSocket* m_curSock;
    QVector<QTcpSocket*> m_msgSockVector;
    unsigned short m_listenPort;

signals:
public slots:
    void onClickedConnect(bool);
    void onSendMsg(bool);
    void onNewConnection();
    void onSelfConnected();
    void onReceiveData();
    void msgSocketError(QAbstractSocket::SocketError sockerror);
    void onSockDisconnected();
};

#endif // CHATFRAME_H
