#ifndef MAINFRAME_H
#define MAINFRAME_H


#include<QPushButton>
#include <QWidget>
#include <QVector>
class ChatFrame;
/**
 * @brief 登入界面
 */
class mainframe : public QWidget
{
    Q_OBJECT

public:
    mainframe(QWidget *parent = 0);
    ~mainframe();
    void init();
protected:
    virtual void closeEvent(QCloseEvent *);
private:
    QPushButton *m_bt_newChatFrame;
    QVector<ChatFrame*> m_chatFrameVector;
signals:
public slots:
    void onNewChatFrame(bool);
};

#endif // MAINFRAME_H
